import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';
import 'dart:ffi';
import 'dart:math';


class PieChartSample extends StatefulWidget {
  final double animatedValue;
  const PieChartSample({super.key, required this.animatedValue});
  @override
  State<PieChartSample> createState() => PieChartState();
}

class PieChartState extends State<PieChartSample> with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  int touchedIndex = -1;
  List<double> numbers = [10,20,30,40];
  late double _previousAnimatedValue = 0;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      duration: const Duration(milliseconds: 500),
      vsync: this,
    );

  }

  @override
  void didUpdateWidget(PieChartSample oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.animatedValue != _previousAnimatedValue) {
      // 处理事件
      // 在这里可以执行任何其他处理逻辑
      // 例如更新UI、触发其他函数等等
      _previousAnimatedValue = widget.animatedValue;
      numbers[0] = widget.animatedValue;
      if (_animationController.status == AnimationStatus.completed) {
        _animationController.reverse();
      }
      else if (_animationController.status == AnimationStatus.dismissed) {
        _animationController.forward();
      }
    }
  }
  void handTap() {
    numbers[0] = widget.animatedValue;
    if (_animationController.status == AnimationStatus.completed) {
      _animationController.reverse();
    }
    else if (_animationController.status == AnimationStatus.dismissed) {
      _animationController.forward();
    }
  }
  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1,
      child: Row(
        children: <Widget>[
          Expanded(
            child: AspectRatio(
              aspectRatio: 1,
              child: AnimatedBuilder(
                animation: _animationController,
                builder: (BuildContext context, Widget? child) {
                  return PieChart(
                    PieChartData(
                      pieTouchData: PieTouchData(
                        touchCallback: (FlTouchEvent event, pieTouchResponse) {
                          setState(() {
                            if (!event.isInterestedForInteractions ||
                                pieTouchResponse == null ||
                                pieTouchResponse.touchedSection == null) {
                              touchedIndex = -1;
                              return;
                            }
                            touchedIndex = pieTouchResponse.touchedSection!.touchedSectionIndex;
                          });
                        },
                      ),
                      borderData: FlBorderData(show: false),
                      sectionsSpace: 0,
                      centerSpaceRadius: 50,
                      sections: showingSections(),
                    ),
                  );
                },
              ),
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 40,child: Text('比亚迪:${numbers[0].toString()}',style:const TextStyle(
                backgroundColor: Colors.blue,
                color: Colors.red,
              ))),
               SizedBox(height: 40,child: Text('哈佛:${numbers[1].toString()}',style:const TextStyle(
                backgroundColor: Colors.yellow,
                color: Colors.red,
              ))),
               SizedBox(height: 40,child: Text('吉利:${numbers[2]}',style:const TextStyle(
                backgroundColor: Colors.purple,
                color: Colors.red,
              )),),
               SizedBox(height: 40,child: Text('奇瑞:${numbers[3]}',style:const TextStyle(
                backgroundColor: Colors.green,
                color: Colors.red,
              )),),
              SizedBox(height: 40,child: Text('update:${widget.animatedValue}',style:const TextStyle(
                backgroundColor: Colors.grey,
                color: Colors.red,
              )),),
            ],
          ),
          const SizedBox(width: 20),
        ],
      ),
    );
  }

  List<PieChartSectionData> showingSections() {
    return List.generate(4, (i) {
      final isTouched = i == touchedIndex;
      final fontSize = isTouched ? 20.0 : 16.0;
      final radius = isTouched ? 80.0 : 60.0;
      return PieChartSectionData(
        color: _getColor(i),
        value:  numbers[i],
        title: _getPercentValueForIndex(i),
        radius: radius,
        titleStyle: TextStyle(
          fontSize: fontSize,
          fontWeight: FontWeight.bold,
          color: Colors.red,
        ),
      );
    });
  }
  Color _getColor(int index) {
    switch (index) {
      case 0:
        return Colors.blue;
      case 1:
        return Colors.yellow;
      case 2:
        return Colors.purple;
      case 3:
        return Colors.green;
      default:
        throw Error();
    }
  }

  // 计算百分比
  String _getPercentValueForIndex(int index) {
    double total = numbers.reduce((value, element) => value + element); // 计算列表中所有元素的总和
    double percentage = (numbers[index] / total) * 100; // 计算每个元素的百分比
    String formattedPercentage = "${percentage.toStringAsFixed(1)}%"; // 将百分比格式化为保留一位小数的字符串，并添加 "%"
    return formattedPercentage;
  }
}