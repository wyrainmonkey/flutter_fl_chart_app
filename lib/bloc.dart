import 'dart:async';

class CounterBloc {
  int _counter = 10;
  final _counterStreamController = StreamController<int>(); // 用于输出计数器值的流
  final _incrementStreamController = StreamController<void>(); // 用于处理增加计数器事件的流

  // 获取计数器值的流
  Stream<int> get counterStream => _counterStreamController.stream;

  // 处理增加计数器事件
  StreamSink<void> get incrementCounter => _incrementStreamController.sink;

  CounterBloc() {
    _incrementStreamController.stream.listen((_) {
      _counter = _counter+5; // 增加计数器值
      _counterStreamController.add(_counter); // 发布新的计数器值
    });
  }

  void dispose() {
    _counterStreamController.close(); // 关闭输出流
    _incrementStreamController.close(); // 关闭输入流
  }
}
