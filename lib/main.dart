

import 'package:flutter/material.dart';
import 'bloc.dart';
import 'PieChar.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final CounterBloc _counterBloc = CounterBloc();
  MyApp({super.key}); // 创建 CounterBloc 实例

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('BLoC Counter'),
        ),
        body: Center(
          child: StreamBuilder<int>(
            stream: _counterBloc.counterStream, // 订阅计数器值的流
            builder: (context, snapshot) {
                return PieChartSample(animatedValue:snapshot.data?.toDouble() ?? 10,);
              // }
            },
          ),
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () => _counterBloc.incrementCounter.add(null), // 发送增加计数器事件
          child: const Icon(Icons.add),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _counterBloc.dispose(); // 关闭 CounterBloc
  }
}

