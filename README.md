# my_app

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://docs.flutter.dev/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://docs.flutter.dev/cookbook)

For help getting started with Flutter development, view the
[online documentation](https://docs.flutter.dev/), which offers tutorials,
samples, guidance on mobile development, and a full API reference.


# 项目名称
my_app
项目简要描述
这个项目主要是绘制了一个饼状图，这个饼状图可以动态更改饼状图某一部分的所占比例大小，可以点击饼状图并实现点击放大，长按放大的功能
具体操作步骤
1、可以点击底部的加号按钮实现动态改变比亚迪的所占比例，比亚迪初始化大小为10 点击加号每次增加5的大小更改所占的比列
2、也可以点击饼状图的某个部分实现放大的动画，单点和长按两种情况实现所不同的放大功能

## 安装和运行
flutter pub add fl_chart 安装依赖包
flutter pub get 实现更新

### 依赖项
sdk: flutter
cupertino_icons: ^1.0.2
fl_chart: ^0.65.0

### 项目的结构和文件组织方式
lib 文件只有三个文件
1、bloc.dart 实现BLoC 模式
2、main.dart 实现主界面的入口
3、PieChart.dart 实现饼状图

### 具体目录讲解
lib目录：这是主要的代码目录，包含了Flutter应用程序的逻辑和业务代码。在lib目录中，可以创建多个子目录来组织代码，如screens（包含屏幕/页面）、widgets（包含可重用的小部件）和models（包含数据模型）等。
android目录：这是Android平台特定的代码目录，包含了Flutter应用程序在Android平台上的配置和特定功能的实现。其中重要的文件夹如下：
app：包含应用程序的配置文件和资源。
manifests：包含Android清单文件。
java：包含Java代码文件。
ios目录：这是iOS平台特定的代码目录，包含了Flutter应用程序在iOS平台上的配置和特定功能的实现。其中重要的文件夹如下：
Runner：包含应用程序的配置文件和资源。
AppDelegate.swift：应用程序的入口文件。
Info.plist：应用程序的信息属性列表文件。
test目录：这是用于编写和运行Flutter应用程序的测试代码的目录。在这个目录中，可以创建多个子目录来组织测试代码。
assets目录：这是用于存储应用程序使用的静态资源（如图像、字体、配置文件等）的目录。在Flutter中，可以通过使用pubspec.yaml文件中的flutter部分来将这些资源添加到项目中。
pubspec.yaml文件：这是应用程序的配置文件，其中列出了项目所依赖的包和资源。在这个文件中，可以指定Flutter SDK版本、应用程序名称、描述、依赖项和资源等。

### 注意点
由于项目比较简单并没有隔离UI,Util,component 三个区域，都放在了lib目录下

### 挑战和方法
fl_chart是一个用于Flutter的图表绘制库，它可以帮助开发者在Flutter应用程序中创建各种类型的图表。在使用fl_chart时，可能会面临以下一些挑战：
数据处理和变换：绘制图表之前，通常需要对数据进行处理和变换，以适应图表的需求。这可能涉及到对数据的筛选、排序、分组或数据类型转换等操作。
绘制复杂的图表：某些图表类型，如饼图、雷达图或瀑布图等，可能需要更复杂的绘制逻辑和算法来实现。这些图表可能需要处理多个数据集、不同的图层绘制以及特定的坐标系转换等。
样式和自定义：在绘制图表时，可能需要对图表的样式进行自定义，以满足特定的设计要求。这可能包括自定义颜色、边框、字体、标签等。

为了克服这些挑战，可以采取以下方法：
数据处理和变换：使用合适的数据处理库，如Dart中的collection库，进行数据处理和变换操作。可以使用各种集合类和函数，如map、sort、filter等，来处理数据。
绘制复杂的图表：熟悉fl_chart库的API文档和示例，了解如何在Flutter应用程序中绘制各种类型的图表。也可以参考其他开源库或图表的实现细节，以获取更多的绘制思路和算法。
样式和自定义：fl_chart库提供了许多自定义选项，例如通过设置Paint对象来自定义颜色和样式，以及通过自定义组件来自定义标签等。可以探索这些选项，并根据需要进行样式和外观的个性化定制。
此外，参与开源社区和与其他开发者的交流是获取解决方案和技术支持的另一种途径。